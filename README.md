# CRAN Indexer

## Prerequisites

* [Docker Compose](https://docs.docker.com/compose/install/)

Navigate to root directory (`cran-indexer`) and run:

* `docker-compose up --build`
* `docker-compose exec --user "$(id -u):$(id -g)" web rails db:reset` (on OSX do not include the `--user` flag)
* `docker-compose exec --user "$(id -u):$(id -g)" web rails db:migrate` (on OSX do not include the `--user` flag)


Import 50 random packages running:
* `docker-compose exec web rake cran:index` (on non OSX include the `--user` flag)


Navigate to front end app.

## Run tests

* `docker-compose run web bundle exec rspec spec`
