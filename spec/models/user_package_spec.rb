require 'rails_helper'

RSpec.describe UserPackage, type: :model do
  it { is_expected.to belong_to :version }
  it { is_expected.to belong_to(:author).class_name("User") }
  it { is_expected.to belong_to(:maintainer).class_name("User") }
  it { is_expected.to validate_presence_of :author }
  it { is_expected.to validate_presence_of :maintainer }

  context "with author present" do
    subject { build(:user_package, :with_author) }

    it { is_expected.to_not validate_presence_of :maintainer }
    it { expect(subject.save!).to be_truthy }
  end

  context "with maintainer present" do
    subject { build(:user_package, :with_maintainer) }

    it { is_expected.to_not validate_presence_of :author }
    it { expect(subject.save!).to be_truthy }
  end

  it "has a unique constrain on [:package_id, :author_id]" do
    existing = create(:user_package, :with_author)
    new_record = existing.dup
    expect { new_record.save! }.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it "has a unique constrain on [:package_id, :maintainer_id]" do
    existing = create(:user_package, :with_maintainer)
    new_record = existing.dup
    expect { new_record.save! }.to raise_error(ActiveRecord::RecordNotUnique)
  end
end
