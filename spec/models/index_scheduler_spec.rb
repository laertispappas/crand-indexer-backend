require 'rails_helper'

describe IndexScheduler, type: :model do
  describe ".run" do
    let(:packages) do
      [
        Cran::Package.new(name: "PackageA", version: "1.1.1"),
        Cran::Package.new(name: "PackageB", version: "2.2.2")
      ]
    end

    before do
      allow_any_instance_of(Cran::Client).to receive(:packages).and_return(packages)
    end

    it "should enqueue a PackageIndexJob for each package found" do
      ActiveJob::Base.queue_adapter = :test

      expect do
        expect {
          described_class.run
        }.to have_enqueued_job(PackageIndexJob).with(name: packages[0].name, version: packages[0].version)
      end.to have_enqueued_job(PackageIndexJob).with(name: packages[1].name, version: packages[1].version)
    end
  end
end
