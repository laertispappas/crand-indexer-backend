require 'rails_helper'

RSpec.describe Package, type: :model do
  subject { build(:package) }

  it { is_expected.to have_many :versions }

  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :title }

  describe "default scope of versions" do
    it "returns versions ordered by version number" do
      package = create(:package)
      first = create(:version, package: package, publication_date: 1.year.ago, version: "1.0.0", most_recent: false)
      last = create(:version, package: package, publication_date: 1.day.ago, version: "2.0.0", most_recent: true)
      second = create(:version, package: package, publication_date: 1.day.ago, version: "1.0.1", most_recent: false)

      expect(package.versions.pluck(:id)).to eq [last.id, second.id, first.id]
    end
  end
  describe "#latest_version" do
    it "returns the most recent published version" do
      package = create(:package)
      create(:version, package: package, publication_date: 1.year.ago, version: "1.0.0", most_recent: false)
      new_version = create(:version, package: package, publication_date: 1.day.ago, version: "2.0.0", most_recent: true)
      create(:version, package: package, publication_date: 1.day.ago, version: "1.0.1", most_recent: false)

      expect(package.latest_version).to eq new_version
    end
  end
end
