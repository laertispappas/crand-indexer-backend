require 'rails_helper'

RSpec.describe Version, type: :model do
  subject { build(:version) }

  it { is_expected.to belong_to :package }
  it { is_expected.to have_many :user_packages }
  it { is_expected.to have_many :authors }
  it { is_expected.to have_many :maintainers }

  it { is_expected.to validate_presence_of :version }
  it { is_expected.to validate_presence_of :publication_date }
  it { is_expected.to validate_presence_of :description }
  it { is_expected.to validate_presence_of :package }
end
