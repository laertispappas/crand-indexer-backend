require "rails_helper"

describe Cran::User, type: :model do

  describe ".from_json" do
    subject { Cran::User.from_json(input) }
    let(:input) { raise(NotImplementedError) }

    context "Given a correct address format" do
      let(:input) { "Some Author <my@author.me>" }

      it "parses the given user address creates a Cran::User" do
        expect(subject.name).to eq "Some Author"
        expect(subject.email).to eq "my@author.me"
      end
    end

    context "Given an incorrect address format" do
      let(:input) { "" }

      it "returns an anonymized Cran::User" do
        expect(subject.name).to match Cran::User::UNKNOWN_USER_PREFIX
        expect(subject.email).to match Cran::User::UNKNOWN_USER_PREFIX
      end
    end

    context "Passing nil" do
      let(:input) { nil }

      it "returns an anonymized Cran::User" do
        expect(subject.name).to match Cran::User::UNKNOWN_USER_PREFIX
        expect(subject.email).to match Cran::User::UNKNOWN_USER_PREFIX
      end
    end

    context "Passing an unprocessed address" do
      let(:input) { "cannot be parsed" }

      it "returns an anonymized Cran::User" do
        expect(subject.name).to match "cannot be parsed"
        expect(subject.email).to match Cran::User::UNKNOWN_USER_PREFIX
      end
    end
  end
end
