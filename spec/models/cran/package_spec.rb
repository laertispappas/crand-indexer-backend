require "rails_helper"

describe Cran::Package, type: :model do
  def dcf_fixture(file)
    JSON.parse(File.read(File.join(Rails.root, "spec", "fixtures", file)))[0]
  end

  describe ".from_json" do
    it "returns a Cran::Package from a PACKAGES parsed file" do
      package = Cran::Package.from_json(dcf_fixture("parsed_responses/PACKAGES.json"))
      expect(package.version).to eq "1.0.0"
      expect(package.name).to eq "A3"
      expect(package.title).to be nil
      expect(package.description).to be nil
      expect(package.publication_date).to be nil
      expect(package.authors).to eq []
      expect(package.maintainers).to eq []
    end

    it "returns a Cran::Package from DESCRIPTION parsed file" do
      package = Cran::Package.from_json(dcf_fixture("parsed_responses/DESCRIPTION.json"))
      expect(package.version).to eq "0.9.6"
      expect(package.name).to eq "ROAuth"

      expect(package.title).to eq "R Interface For OAuth"
      expect(package.description).to include "OAuth 1.0 specification allowing users"
      expect(package.publication_date).to eq Time.parse("2015-02-13 20:32:47").iso8601

      expect(package.authors[0].name).to eq "Jeff Gentry"
      expect(package.authors[0].email).to eq "geoffjentry@gmail.com"
      expect(package.authors[1].name).to eq "Duncan Temple Lang"
      expect(package.authors[1].email).to eq "duncan@r-project.org"

      expect(package.maintainers[0].name).to eq "Pablo Barbera"
      expect(package.maintainers[0].email).to eq "pablo.barbera@nyu.edu"
    end
  end
end
