require "rails_helper"

describe Cran::Client, type: :model do
  def dcf_ficture_path_as_hash(file)
    JSON.parse(File.read(dcf_fixture_path(file)))
  end

  def dcf_fixture_path(file)
    File.expand_path(File.join(Rails.root, "spec", "fixtures", file))
  end

  describe "#get" do
    let(:package_name) { "ROAuth" }
    let(:package_version) { "0.9.4" }
    let(:expected_package_details) { dcf_ficture_path_as_hash("parsed_responses/DESCRIPTION.json")[0] }

    # the tests here are kind of redundant. Since we expose and depend on Cran::Package
    # here and to the importer. We can test only that Cran::Package gets the message
    # `from_json` with the correct fixture sample.
    #
    # Various DESCRIPTION samples should be tested autonomously in Cran::Package class.
    # I would do the refactoring later if I have some time. :)
    #
    it "returns a Cran::Package model from the package description" do
      cran_package = subject.get(name: package_name, version: package_version)

      expect(cran_package.name).to eq expected_package_details["Package"]
      expect(cran_package.version).to eq expected_package_details["Version"]
      expect(cran_package.title).to eq expected_package_details["Title"]
      expect(cran_package.description).to eq(expected_package_details["Description"])
      expect(cran_package.publication_date).to eq Time.parse(expected_package_details["Date/Publication"]).iso8601
      expect(cran_package.authors.size).to eq 2

      authors = cran_package.authors
      expect(authors[0].name).to eq "Jeff Gentry"
      expect(authors[0].email).to eq "geoffjentry@gmail.com"
      expect(authors[1].name).to eq "Duncan Temple Lang"
      expect(authors[1].email).to eq "duncan@r-project.org"

      maintainers = cran_package.maintainers
      expect(maintainers[0].name).to eq "Pablo Barbera"
      expect(maintainers[0].email).to eq "pablo.barbera@nyu.edu"
    end
  end

  describe "#packages" do
    it "returns a collection of Cars::Package models from PACKAGES Debian Control File" do
      packages_as_hash = dcf_ficture_path_as_hash("parsed_responses/PACKAGES.json")

      packages = subject.packages
      expect(packages.size).to eq 10

      # we only parse the name and the version from PACKAGES file
      #
      packages_as_hash.each_with_index do |hash, index|
        expect(packages[index].name).to eq hash["Package"]
        expect(packages[index].version).to eq hash["Version"]
      end
    end
  end
end
