require "rails_helper"

describe Importer, type: :model do
  subject(:importer) { Importer.new }

  let(:authors) do
    [
      Cran::User.new(name: "Author A", email: "AuthorA@example.com"),
      Cran::User.new(name: "Author B", email: "AuthorB@example.com")
    ]
  end

  let(:maintainers) do
    [
      Cran::User.new(name: "Maintainer A", email: "MaintainerA@example.com"),
    ]
  end

  let(:import) do
    Cran::Package.new(name: "r-oauth",
                      title: "A simple R package",
                      version: "1.0.0",
                      publication_date: "2012-08-14 16:27:09",
                      description: "Some longer description than title",
                      authors: authors,
                      maintainers: maintainers)
  end
  let(:package) { Package.find_by_name import.name }

  describe "#call" do
    context "importing a new package" do
      before do
        importer.call(import)
      end

      it "stores a package" do
        expect(package).to be_persisted
        expect(package.name).to eq import.name
        expect(package.title).to eq import.title
      end

      it "stores a version for the package" do
        expect(package.versions.count).to eq 1
        version = package.versions.first

        expect(version.version).to eq import.version
        expect(version.description).to eq import.description
        expect(version.publication_date).to eq import.publication_date
        expect(version).to be_most_recent
      end

      it "stores the authors" do
        expect(package.versions[0].authors.count).to eq 2
        version = package.versions[0]
        expect(version.authors[0].name).to eq authors[0].name
        expect(version.authors[0].email).to eq authors[0].email
        expect(version.authors[1].name).to eq authors[1].name
        expect(version.authors[1].email).to eq authors[1].email
      end

      it "stores the maintainers" do
        expect(package.versions[0].maintainers.count).to eq 1

        version = package.versions[0]
        expect(version.maintainers[0].name).to eq maintainers[0].name
        expect(version.maintainers[0].email).to eq maintainers[0].email
      end
    end

    context "importing a new version of the package" do
      let(:new_import) do
        import.dup.tap do |import|
          import.version = "0.0.1"
          import.publication_date = "2017-08-14 16:27:09"
          import.description = "New descirption"
        end
      end

      before do
        importer.call(import)
        importer.call(new_import)
      end

      it "should store both versions for the same package" do
        expect(package.versions.count).to eq 2

        first_version = package.versions.first
        expect(first_version.version).to eq import.version
        expect(first_version.description).to eq import.description
        expect(first_version.publication_date).to eq import.publication_date
        expect(first_version).to be_most_recent

        second_version = package.versions.last
        expect(second_version.version).to eq new_import.version
        expect(second_version.description).to eq new_import.description
        expect(second_version.publication_date).to eq new_import.publication_date
        expect(second_version).to_not be_most_recent

        expect(package.versions[0].authors.count).to eq 2
      end

      it "stores authors references for both packages" do
        first_version = package.versions[0]
        expect(first_version.authors.count).to eq 2

        second_version = package.versions[1]
        expect(second_version.authors.count).to eq 2
      end

      it "stores maintainers references for both packages" do
        first_version = package.versions[0]
        expect(first_version.maintainers.count).to eq 1

        second_version = package.versions[1]
        expect(second_version.maintainers.count).to eq 1
      end

      it "authors from both versions reference to the same users" do
        expect(package.versions.map(&:authors).flat_map(&:ids).uniq.size).to eq 2
      end

      it "maintainers from both versions reference to the same user" do
        expect(package.versions.map(&:maintainers).flat_map(&:ids).uniq.size).to eq 1
      end

      context "importing a new version with an extra author" do
        let(:new_author) { Cran::User.new(name: "New author", email: "new@exmaple.com") }
        let(:new_version) { package.versions.find_by_version "9.9.9" }
        let(:old_version) { package.versions.find_by_version import.version }

        before do
          new_import.version = "9.9.9"
          new_import.authors << new_author
          importer.call(new_import)
        end

        it "stores all the authors" do
          expect(new_version.authors.size).to eq 3
          expect(old_version.authors.size).to eq 2
        end

        it "authors from all versions reference to the same users" do
          expect(package.versions.map(&:authors).uniq.size).to eq 3
        end

        it "stores the new version with the new author" do
          last_user = new_version.authors.last

          expect(last_user.name).to eq new_author.name
          expect(last_user.email).to eq new_author.email
        end
      end

      context "importing a new version with an extra maintainer" do
        let(:new_maintainer) { Cran::User.new(name: "New maintainer", email: "new_maintainer@exmaple.com") }
        let(:new_version) { package.versions.find_by_version new_import.version }
        let(:old_version) { package.versions.find_by_version import.version }

        before do
          new_import.version = "9.9.9"
          new_import.maintainers << new_maintainer
          importer.call(new_import)
        end

        it "stores all the maintainers" do
          expect(new_version.maintainers.count).to eq 2
          expect(old_version.maintainers.count).to eq 1
        end

        it "stores the new version with the cerrect maintainer details" do
          last_user = new_version.maintainers.last # :)

          expect(last_user.name).to eq new_maintainer.name
          expect(last_user.email).to eq new_maintainer.email
        end
      end

      context "importing a new version when an author is missing" do
        let(:new_version) { package.versions.find_by_version new_import.version }
        let(:old_version) { package.versions.find_by_version import.version }

        before do
          new_import.version = "9.9.9"
          new_import.authors.pop
          importer.call(new_import)
        end

        it "stores the previous version authors" do
          expect(old_version.authors.size).to eq 2
        end

        it "stores the new version with only one author" do
          expect(new_version.authors.size).to eq 1

          last_user = new_version.authors.last # :)
          expect(last_user.name).to eq new_import.authors.first.name
          expect(last_user.email).to eq new_import.authors.first.email
        end
      end

      context "importing a new version when a maintainer is missing" do
        before do
          new_import.version = "9.9.9"
          new_import.maintainers.pop
          importer.call(new_import)
        end

        let(:new_version) { package.versions.find_by_version new_import.version }
        let(:old_version) { package.versions.find_by_version import.version }

        it "stores the maintainer of the previous version" do
          expect(old_version.maintainers.count).to eq 1
        end

        it "stores the new version without any maintaners" do
          expect(new_version.maintainers.count).to eq 0
        end
      end
    end

    context "importing an already existing package" do
      before do
        subject.call(import)
        subject.call(import)
      end

      it "stores only one version for the package" do
        expect(package.versions.count).to eq 1
      end

      it "stores only 2 authors and 1 maintainer" do
        expect(package.versions.first.authors.count).to eq 2
        expect(package.versions.first.maintainers.count).to eq 1
      end
    end

    context "on package creation failure" do
      let(:import) do
        Cran::Package.new(name: "r-oauth",
                          title: "A simple R package",
                          version: "",
                          publication_date: "",
                          description: "")
      end

      it "does not store any packages" do
        expect { importer.call(import).to not_change (Package.count) }
      end

      it "does not store any versions" do
        expect { importer.call(import).to not_change(Version.count) }
      end

      it "does not store any User" do
        expect { importer.call(import).to not_change(User.count) }
      end
    end

    context "on version creation failure" do
      let(:import) do
        Cran::Package.new(name: "",
                          title: "",
                          version: "1.1.1",
                          publication_date: "2012-08-14 16:27:09",
                          description: "Some longer description than title")
      end

      it "does not store any packages" do
        expect { importer.call(import).to not_change(Package.count) }
      end

      it "does not store any versions" do
        expect { importer.call(import).to not_change(Version.count) }
      end

      it "does not store any users" do
        expect { importer.call(import).to not_change(User.count) }
      end
    end

    context "on user creation failure" do
      let(:import) do
        Cran::Package.new(name: "Package name",
                          title: "Package title",
                          version: "1.1.1",
                          publication_date: "2012-08-14 16:27:09",
                          description: "Some longer description than title",
                          authors: [Cran::User.new])
      end

      it "does not store any packages" do
        expect { importer.call(import).to not_change(Package.count) }
      end

      it "does not store any versions" do
        expect { importer.call(import).to not_change(Version.count) }
      end

      it "does not store any users" do
        expect { importer.call(import).to not_change(User.count) }
      end
    end
  end
end
