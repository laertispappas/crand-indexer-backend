FactoryBot.define do
  factory :user_package do
    version

    trait :with_author do
      after(:build) do |user_package|
        user_package.author = create(:user)
      end
    end
  end

  trait :with_maintainer do
    after(:build) do |user_package|
      user_package.maintainer = create(:user)
    end
  end
end
