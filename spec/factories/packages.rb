FactoryBot.define do
  factory :package do
    name { "OAuth" }
    title { "OAuth for R" }
  end
end
