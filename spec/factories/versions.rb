FactoryBot.define do
  factory :version do
    package
    version { "1.1.1" }
    publication_date { Time.now }
    description { "some long description" }
  end
end
