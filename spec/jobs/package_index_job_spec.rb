require 'rails_helper'

RSpec.describe PackageIndexJob, type: :job do
  # See fixtures under spec/fixtures
  #
  let(:package_name) { "ROAuth" }
  let(:package_version) { "0.9.4" }

  describe "#perform" do
    it "gets the remote package details and imports it locally" do
      cran_package = instance_double(Cran::Package)
      client = instance_double(Cran::Client)

      allow(Cran::Client).to receive(:new).and_return(client)
      allow(client).to receive(:get).with(name: package_name, version: package_version).and_return(cran_package)

      importer = instance_double(Importer)
      allow(Importer).to receive(:new).and_return(importer)

      expect(importer).to receive(:call).with(cran_package)

      PackageIndexJob.perform_now(name: package_name, version: package_version)
    end

    it "a package and its version should be created" do
      expect do
        expect do
          PackageIndexJob.perform_now(name: package_name, version: package_version)
        end.to change { Package.count }.by(1)
      end.to change { Version.count }.by(1)
    end
  end
end
