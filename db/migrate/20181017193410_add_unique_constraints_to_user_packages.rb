class AddUniqueConstraintsToUserPackages < ActiveRecord::Migration[5.2]
  def change
    add_index :user_packages, [:version_id, :author_id], unique: true
    add_index :user_packages, [:version_id, :maintainer_id], unique: true
  end
end
