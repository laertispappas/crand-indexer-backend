class AddMostRecentToVersions < ActiveRecord::Migration[5.2]
  def change
    add_column :versions, :most_recent, :boolean, default: true
  end
end
