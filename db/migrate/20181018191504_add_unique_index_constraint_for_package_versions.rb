class AddUniqueIndexConstraintForPackageVersions < ActiveRecord::Migration[5.2]
  def change
    add_index :versions, [:package_id, :version], unique: true
  end
end
