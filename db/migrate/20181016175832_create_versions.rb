class CreateVersions < ActiveRecord::Migration[5.2]
  def change
    create_table :versions do |t|
      t.references :package, index: true

      t.string :version, null: false
      t.datetime :publication_date, null: false
      t.text :description, null: false

      t.timestamps
    end

    add_index :versions, [:package_id, :version], unique: true
  end
end
