class AddCheckConstraintForInUserPackages < ActiveRecord::Migration[5.2]
  def up
    sql = <<~SQL
      ALTER TABLE user_packages
      ADD CONSTRAINT check_user_reference_mutual_exclusive
      CHECK (
        (author_id IS NULL AND maintainer_id IS NOT NULL) OR
        (author_id IS NOT NULL AND maintainer_id IS NULL)
      )
    SQL
    execute sql
  end

  def down
    sql = <<~SQL
      ALTER TABLE user_packages
      DROP CONSTRAINT check_user_reference_mutual_exclusive
    SQL
    execute sql
  end
end
