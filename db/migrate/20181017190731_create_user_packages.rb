class CreateUserPackages < ActiveRecord::Migration[5.2]
  def change
    create_table :user_packages do |t|
      t.bigint :author_id, references: :users
      t.bigint :maintainer_id, references: :users
    end

    add_foreign_key :user_packages, :users, column: :author_id
    add_foreign_key :user_packages, :users, column: :maintainer_id
  end
end
