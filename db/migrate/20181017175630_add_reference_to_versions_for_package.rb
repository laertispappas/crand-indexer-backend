class AddReferenceToVersionsForPackage < ActiveRecord::Migration[5.2]
  def up
    remove_reference :versions, :package
    add_reference :versions, :package, foreign_key: true, index: true
  end

  def down
    remove_reference :versions, :package
    add_reference :versions, :package, index: true
  end
end
