class AddIndexOnVersionsVersionColumn < ActiveRecord::Migration[5.2]
  def change
    add_index :versions, :version
  end
end
