class AddVersionIdToUserPackages < ActiveRecord::Migration[5.2]
  def change
    add_reference :user_packages, :version, null: false
    add_foreign_key :user_packages, :versions
  end
end
