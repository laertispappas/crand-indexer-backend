SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: packages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE packages (
    id bigint NOT NULL,
    name character varying NOT NULL,
    title character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: packages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE packages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: packages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE packages_id_seq OWNED BY packages.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: user_packages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_packages (
    id bigint NOT NULL,
    author_id bigint,
    maintainer_id bigint,
    version_id bigint NOT NULL,
    CONSTRAINT check_user_reference_mutual_exclusive CHECK ((((author_id IS NULL) AND (maintainer_id IS NOT NULL)) OR ((author_id IS NOT NULL) AND (maintainer_id IS NULL))))
);


--
-- Name: user_packages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_packages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_packages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_packages_id_seq OWNED BY user_packages.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id bigint NOT NULL,
    email character varying NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE versions (
    id bigint NOT NULL,
    version character varying NOT NULL,
    publication_date timestamp without time zone NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    package_id bigint,
    most_recent boolean DEFAULT true
);


--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE versions_id_seq OWNED BY versions.id;


--
-- Name: packages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY packages ALTER COLUMN id SET DEFAULT nextval('packages_id_seq'::regclass);


--
-- Name: user_packages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_packages ALTER COLUMN id SET DEFAULT nextval('user_packages_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: versions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions ALTER COLUMN id SET DEFAULT nextval('versions_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: packages packages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY packages
    ADD CONSTRAINT packages_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: user_packages user_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_packages
    ADD CONSTRAINT user_packages_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: versions versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: index_packages_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_packages_on_name ON public.packages USING btree (name);


--
-- Name: index_user_packages_on_version_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_packages_on_version_id ON public.user_packages USING btree (version_id);


--
-- Name: index_user_packages_on_version_id_and_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_user_packages_on_version_id_and_author_id ON public.user_packages USING btree (version_id, author_id);


--
-- Name: index_user_packages_on_version_id_and_maintainer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_user_packages_on_version_id_and_maintainer_id ON public.user_packages USING btree (version_id, maintainer_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_versions_on_package_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_versions_on_package_id ON public.versions USING btree (package_id);


--
-- Name: index_versions_on_package_id_and_version; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_versions_on_package_id_and_version ON public.versions USING btree (package_id, version);


--
-- Name: index_versions_on_version; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_versions_on_version ON public.versions USING btree (version);


--
-- Name: versions fk_rails_147d97cf36; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT fk_rails_147d97cf36 FOREIGN KEY (package_id) REFERENCES packages(id);


--
-- Name: user_packages fk_rails_1ede195fa8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_packages
    ADD CONSTRAINT fk_rails_1ede195fa8 FOREIGN KEY (maintainer_id) REFERENCES users(id);


--
-- Name: user_packages fk_rails_71b73ded40; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_packages
    ADD CONSTRAINT fk_rails_71b73ded40 FOREIGN KEY (version_id) REFERENCES versions(id);


--
-- Name: user_packages fk_rails_d2cdbb86c6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_packages
    ADD CONSTRAINT fk_rails_d2cdbb86c6 FOREIGN KEY (author_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20181016174454'),
('20181016175832'),
('20181017174145'),
('20181017175630'),
('20181017190731'),
('20181017192117'),
('20181017192958'),
('20181017193410'),
('20181018191504'),
('20181019150732'),
('20181019215450');


