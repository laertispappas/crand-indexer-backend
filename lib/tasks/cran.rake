namespace :cran do
  desc "Start index processing"
  task index: :environment do
    IndexScheduler.run
  end
end
