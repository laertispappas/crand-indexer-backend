class PackagesController < ActionController::API
  # TODO: Pagination / tests
  #
  def show
    version = Package.find_by!(name: params[:name])
                .versions.includes(:package, :authors, :maintainers)
                .find_by!(version: params[:version])

    render json: version, serializer: VersionDetailsSerializer
  end

  def index
    render json: Version.includes(:package, :authors, :maintainers).where(most_recent: true)
  end
end
