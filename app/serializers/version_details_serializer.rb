class VersionDetailsSerializer < VersionSerializer
  attributes :children

  def children
    object.package.versions.pluck(:version).delete_if { |v| v == object.version }.map do |v|
      { version: v }
    end
  end
end
