class VersionSerializer < ActiveModel::Serializer
  has_many :authors
  has_many :maintainers

  attributes :name, :title, :description, :version, :authors, :maintainers

  def name
    object.package.name
  end

  def title
    object.package.title
  end

end
