class Importer
  # Assumes packages are immutable once published or else the code needs ot be adapted
  #
  def call(import)
    ApplicationRecord.transaction do
      package = find_or_create_package!(import)
      version = find_or_create_version!(package, import)
      find_or_create_people!(import.authors) do |user|
        version.user_packages.find_or_create_by!(author: user)
      end
      find_or_create_people!(import.maintainers) do |user|
        version.user_packages.find_or_create_by!(maintainer: user)
      end
    end
  rescue ActiveRecord::RecordInvalid => _ex
    false
  end

  private

  def find_or_create_package!(import)
    Package.find_or_create_by!(name: import.name) { |package| package.title = import.title }
  end

  # Assumes packages are immutable
  def find_or_create_people!(cran_people)
    Rails.logger.warn "no people found" if cran_people.blank?

    cran_people.each do |cran_person|
      user = User.find_or_create_by!(email: cran_person.email) do |user|
        user.name = cran_person.name
      end
      yield user
    end
  end

  def find_or_create_version!(package, import)
    is_import_latest_version = latest_version?(package, import)

    if is_import_latest_version && package.latest_version
      package.latest_version.update!(most_recent: false)
    end

    package.versions.find_or_create_by!(version: import.version) do |version|
      version.publication_date = import.publication_date
      version.description = import.description
      version.most_recent = is_import_latest_version
    end
  end

  def latest_version?(package, import)
    return true unless package.latest_version

    import.version > package.latest_version.version
  end
end
