class UserPackage < ApplicationRecord
  belongs_to :version
  belongs_to :author, class_name: "User", foreign_key: "author_id", optional: true
  belongs_to :maintainer, class_name: "User", foreign_key: "maintainer_id", optional: true

  validates_presence_of :author, unless: :maintainer_id
  validates_presence_of :maintainer, unless: :author_id
end
