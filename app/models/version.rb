class Version < ApplicationRecord
  belongs_to :package

  has_many :user_packages
  has_many :authors, through: :user_packages
  has_many :maintainers, through: :user_packages

  default_scope { order(version: :desc) }

  scope :most_recent, -> { where(most_recent: true) }

  validates_presence_of :version, :publication_date, :description, :package
end
