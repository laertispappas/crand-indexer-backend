class Package < ApplicationRecord
  has_many :versions, dependent: :destroy

  validates_presence_of :name, :title

  def latest_version
    versions.most_recent.first
  end
end
