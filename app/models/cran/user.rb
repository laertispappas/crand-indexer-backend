require "mail"

module Cran
  class User
    UNKNOWN_USER_PREFIX = "CRAN_INDEXER_UNKNOWN_USER_"
    include ActiveModel::Model

    attr_accessor :name, :email

    def self.from_json(author_address)
      # TODO: Some packages do not contain the correct
      # address format. So this will brake. Also some
      # packages contain the following format: name1, name2 and name3
      # which cannot be parsed because of `and` keyword. Currently
      # we split by comma so we either need to parse the logic or skip
      # at all. Moreover some descriptions do not contain an email
      # address which is an error in AR User model (null: false)
      #
      address = Mail::Address.new(author_address)
      new(name: address.name.presence || gen_rand, email: address.address || gen_rand)
    rescue Mail::Field::IncompleteParseError => _ex
      # return an unrecognized user. How to handle this depends on the business logic
      # I would prefer to keep the null constraints in the user model and return some
      # anonymized users instead.
      new(name: author_address, email: gen_rand)
    end

    def self.gen_rand
      UNKNOWN_USER_PREFIX + SecureRandom.hex(10)
    end

    private_class_method :gen_rand

  end
end
