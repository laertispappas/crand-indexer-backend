module Cran
  class Extractor
    TEMP_EXTRACTION_DIR = "./tmp/cran-indexer"

    def initialize(fetcher, file_for_extraction)
      @fetcher = fetcher
      @file_for_extraction = file_for_extraction

      FileUtils.mkdir_p(TEMP_EXTRACTION_DIR) unless File.directory?(TEMP_EXTRACTION_DIR)
    end

    def call
      fetcher.call
      extract_file!
    end

    def read
      File.read(extracted_file)
    end

    def cleanup
      fetcher.cleanup
      FileUtils.rm_rf(extracted_directory)
    end

    private

    attr_reader :fetcher, :file_for_extraction

    def extract_file!
      assert! do
        system "tar -zxvf #{tar_path} -C #{TEMP_EXTRACTION_DIR} #{file_for_extraction}"
      end
    end

    def extracted_file
      [TEMP_EXTRACTION_DIR, file_for_extraction].join("/")
    end

    def extracted_directory
      extracted_file_parent_dir = file_for_extraction.split("/")[0]
      "#{TEMP_EXTRACTION_DIR}/#{extracted_file_parent_dir}"
    end

    def tar_path
      fetcher.destination.path
    end

    def assert!
      unless yield
        raise "Could not extract file: #{tar_path}" unless bool
      end
    end
  end
end
