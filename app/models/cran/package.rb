module Cran
  class Package
    include ActiveModel::Model

    attr_accessor :name,
                  :version,
                  :title,
                  :description,
                  :publication_date,
                  :authors,
                  :maintainers

    def self.from_json(hash)
      new(name: hash["Package"],
          version: hash["Version"],
          title: hash["Title"],
          description: hash["Description"],
          publication_date: parse_time(hash["Date/Publication"]),
          authors: build_people(hash["Author"]),
          maintainers: build_people(hash["Maintainer"]))
    end

    def self.parse_time(time)
      time && Time.parse(time).iso8601
    end

    private_class_method :parse_time

    def self.build_people(authors)
      return [] unless authors.present?

      authors
        .split(",")
        .flat_map(&:strip)
        .reject(&:blank?)
        .map(&method(:to_author))
    end
    private_class_method :build_people

    def self.to_author(author)
      Cran::User.from_json(author)
    end

    private_class_method :to_author
  end
end
