require "open-uri"

module Cran
  class Client
    # Dcf library is hanging when the code is running in docker and it also
    # slow and memory consumtion is increased in local machine as well. The parsing
    # logic is not perfect though is assumes that each entry is in a new line. There
    # are occasions where this is not true though (a value may be split in multiple lines).
    # For example:
    # Imports: readr, MASS, matrixStats, ranger, parallel, stringr, Rcpp (>=
    #         0.11.2)
    class Parser
      def self.parse(content)
        new_package = {}
        result = []

        content.split("\n").each do |entry|
          next unless entry.present?

          # On new entry
          if entry.start_with?("Package") && new_package.present?
            result << new_package
            new_package = {}
          end

          new_package[entry.split(": ")[0]] = entry.split(": ")[1]
        end

        # Make sure we get the last entry
        result << new_package
        result
      end
    end

    def packages
      fetcher = Fetcher.new(packages_path)
      fetcher.call

      Parser.parse(fetcher.read).map(&method(:to_model)).tap do
        fetcher.cleanup
      end
    end

    def get(name:, version:)
      extractor = Extractor.new(
        Fetcher.new(remote_package_path(name, version)),
        file_to_extract(name)
      )
      extractor.call
      description = extractor.read
      extractor.cleanup

      Dcf.parse(description).map(&method(:to_model)).first
    end

    private

    def base_path
      Rails.env.test? && File.join(Rails.root, "spec", "fixtures") || "https://cran.r-project.org/src/contrib"
    end

    def to_model(response)
      Cran::Package.from_json(response)
    end

    def packages_path
      "#{base_path}/PACKAGES"
    end

    def remote_package_path(name, version)
      File.join(base_path, full_package_name(name, version))
    end

    def full_package_name(name, version)
      "#{name}_#{version}.tar.gz"
    end

    def file_to_extract(name)
      [name, "DESCRIPTION"].join("/")
    end
  end
end
