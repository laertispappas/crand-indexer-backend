module Cran
  class Fetcher
    attr_reader :source, :destination

    def initialize(source)
      @source = source
      @destination = Tempfile.new
    end

    def call
      IO.copy_stream(open(source), destination.path)
    end

    def read
      File.read(destination)
    end

    def cleanup
      destination.close
      destination.unlink
    end
  end
end
