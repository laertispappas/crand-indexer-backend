class IndexScheduler
  # TODO: Test for non test env
  INDEX_LIMIT_SIZE = Rails.env.test? && 2 ||
    ENV["CRAN_INDEX_LIMIT_SIZE"].presence && ENV["CRAN_INDEX_LIMIT_SIZE"].to_i ||
    50

  def self.run
    Cran::Client.new.packages.sample(INDEX_LIMIT_SIZE).each do |cran_package|
      PackageIndexJob.perform_later name: cran_package.name, version: cran_package.version
    end
  end
end
