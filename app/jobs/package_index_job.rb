class PackageIndexJob < ApplicationJob
  queue_as :default

  def perform(name:, version:)
    cran_package = Cran::Client.new.get(name: name, version: version)
    Importer.new.call(cran_package)
  end
end
