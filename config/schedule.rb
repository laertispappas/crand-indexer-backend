#
# On distributed systems we need to make sure that this will run by only 1 process. Otherwise
# the implementation of Index scheduling needs to be changed to take this into account.
#
#
every 1.day, at: '12:00 pm' do
  runner "IndexScheduler.run"
end
