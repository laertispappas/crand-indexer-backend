Rails.application.routes.draw do
  resources :packages, only: [:index, :show], param: :name
end
